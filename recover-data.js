/**
 * RECUPERA AS INFORMAÇÕES DO DATASET -> NECESSÁRIO PASSAR O 'ID' E OS ATRIBUTOS DO DISPOSITIVO
 * curl -X GET \
 * -H "Authorization: Bearer {TOKEN_JWT}" \
 * "http://localhost:8000/history/device/ {id} /history? {lastN=3 ou vazio} &attr= {attribute}"
 */

var shell = require('shelljs');
var fileSystem = require('fs')

// ubuntu server - labex
const URL = "http://10.211.2.253:8000";
// ID do dispositivo
const deviceId = "c0d2f3";
const attr = "temp";
// const lastNumber = "500";
// const attrLastNumbers = `lastN=${lastNumber}`
// const query = `${attrLastNumbers}&${attr}`

fileSystem.readFile('./token.json', 'utf8', function (err, data){
  if(err){
    console.log("Erro ao carregar arquivo");
  } else{
    const token = JSON.parse(data);
    // console.log(token.jwt);
    
    const comand = `curl -X GET \
      -H "Authorization: Bearer ${token.jwt}" \
      "${URL}/history/device/${deviceId}/history?attr=${attr}"`;
    
    // EXECUTA REQUISIÇÃO AO DOJOT E SALVA DADOS EM ARQUIVO - [.JSON] -
    // shell.echo(shell.exec(comand)).toEnd(`data-${attr}.json`);
    shell.exec(comand).toEnd(`data-${attr}.json`);

    // TRANSFORMA DADOS RECUPERADOS DO DOJOT - [.JSON] - EM ARQUIVO - [.CSV] -
    // shell.exec('json2csv -i data-temp.json -f device_id,ts,value,attr -o out.csv');
  }
});

