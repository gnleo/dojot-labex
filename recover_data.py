import os
import pandas as pd

dataFrame = pd.read_json('data.json').drop(columns=['device_id'])
dataFrame.to_csv('data.csv')
os.system('rm -r data.json')