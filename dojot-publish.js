const mqtt = require('mqtt');
const express = require('express');

console.log('running');

// ubuntu server - labex
const client  = mqtt.connect('mqtt://10.211.2.253');
const app = express();

app.use(express.json());

client.on('connect', () => {
  
  console.log('connected mqtt');

  app.post('/publish', (request, response) => {
    console.log('access route');
    // console.log(request.body);
    
    const { device_id, message } = request.body;

    client.subscribe(`/admin/${device_id}/attrs`);
    client.publish(`/admin/${device_id}/attrs`, `{"message": "${message}"}`);
    return response.status(201).json({message: 'success'});
  });

  // return response.status(400).json({message: 'error', description: 'Not connected'});

});

app.listen(8080);
  