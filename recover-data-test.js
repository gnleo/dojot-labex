/**
 * RECUPERA AS INFORMAÇÕES DO DATASET -> NECESSÁRIO PASSAR O 'ID' E OS ATRIBUTOS DO DISPOSITIVO
 * curl -X GET \
 * -H "Authorization: Bearer {TOKEN_JWT}" \
 * "http://localhost:8000/history/device/ {id} /history? {lastN=3 ou vazio} &attr= {attribute}"
 */

var shell = require('shelljs');

const tokenJwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJoWFVzY01lVUFpaXlwb3pOdnField6UTVYd04xQ1puUyIsImlhdCI6MTU3NDQ1MTczMywiZXhwIjoxNTc0NDUyMTUzLCJuYW1lIjoiQWRtaW4gKHN1cGVydXNlcikiLCJlbWFpbCI6ImFkbWluQG5vZW1haWwuY29tIiwicHJvZmlsZSI6ImFkbWluIiwiZ3JvdXBzIjpbMV0sInVzZXJpZCI6MSwianRpIjoiYmRjNzg1MzFmNjk3NjkyOGI3OWU2MTc0YzE5MzBlMjAiLCJzZXJ2aWNlIjoiYWRtaW4iLCJ1c2VybmFtZSI6ImFkbWluIn0.BtiFvxHHgs1Aj69kHTD0qKgqpylFiQjvfFd-SU2cu5c";
const baseUrl = "http://localhost:8000";

const deviceId = "4d8d4a";

const lastNumber = "5";
const attrLastNumbers = `lastN=${lastNumber}`
const queryAll = "attr=Temperatura&attr=Umidade";
const queryCustom = `${attrLastNumbers}&${queryAll}`

const comand = `curl -X GET \
-H "Authorization: Bearer ${tokenJwt}" \
"${baseUrl}/history/device/${deviceId}/history?${queryCustom}"`;

// EXECUTA REQUISIÇÃO AO DOJOT E SALVA DADOS EM ARQUIVO - [.JSON] -
// shell.echo(shell.exec(comand)).toEnd('data.json');

// EXECUTA REQUISIÇÃO AO DOJOT E TRANSFORMANDOS OS DADOS RECUPERADOS EM [JSON]
var data = JSON.parse(shell.exec(comand));

console.log('\n\nDATA RECEVED\n');
console.log(data[0].value);

// CONVERTE DADOS RECUPERADOS DO DOJOT PARA FORMATO MATRICIAL
// const { parse } = require('json2csv');
// const fields = ["device_id", "ts", "value", "attr"];
// try {
//     const csv = parse(data, fields);
//     console.log(csv);
// } catch (err) {
//     console.error(err);
// }

// TRANSFORMA DADOS RECUPERADOS DO DOJOT - [.JSON] - EM ARQUIVO - [.CSV] -
// shell.exec('json2csv -i /home/labex/Documentos/ProjetoOnibusComfiguracao/data.json -f device_id,ts,value,attr -o out.csv');