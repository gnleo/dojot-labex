const mqtt = require('mqtt');
const express = require('express');
const {base64decode} = require('nodejs-base64');

console.log('running');

// ubuntu server - labex
const client  = mqtt.connect('mqtt://10.211.2.253');

const app = express();
app.use(express.json());

client.on('connect', () => {

  console.log('connected mqtt');

  app.post('/publish', (request, response) => {

    console.log('access route');
    console.log(request.body);

    const { applicationID, data } = request.body;

    if(applicationID == '1'){

      var device_id = "a42676";
      var decode = base64decode(data);
      
      attr = decode.split(" ");

      var luz = parseFloat(attr[1]);
      var temp = parseFloat(attr[2]);
      var umidade =  parseFloat(attr[3]);

      client.subscribe(`/admin/${device_id}/attrs`);
      client.publish(`/admin/${device_id}/attrs`, `{"luz":${luz}}`);
      client.publish(`/admin/${device_id}/attrs`, `{"temp":${temp}}`);
      client.publish(`/admin/${device_id}/attrs`, `{"umidade":${umidade}}`);

    } else {
      var device_id = "38cc48";
      client.subscribe(`/admin/${device_id}/attrs`);
      client.publish(`/admin/${device_id}/attrs`, `{"data":"${data}"}`);
    }


//    client.subscribe(`/admin/${device_id}/attrs`);
//    client.publish(`/admin/${device_id}/attrs`, `{"applicationName":"${applicationName}"}`);
//    client.publish(`/admin/${device_id}/attrs`, `{"deviceName":"${deviceName}"}`);
//    client.publish(`/admin/${device_id}/attrs`, `{"timestamp":"${timestamp}"}`);
//    client.publish(`/admin/${device_id}/attrs`, `{"data":"${data}"}`);

    return response.status(201).json({message: 'success'});
  });

  // return response.status(400).json({message: 'error', description: 'Not connected'});

});

app.listen(8080);
